﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Linq;
namespace Assignment1_B
{

    public partial class Default : System.Web.UI.Page
    {
        public void Submit(object sender, EventArgs args)
        {
            string first_name = clientFname.Text.ToString();
            string last_name = clientLname.Text.ToString();
            string DOB = clientDOB.Text.ToString();
            string address = clientAddress.Text.ToString();
            string city = clientCity.Text.ToString();
            string postalCode = clientPostalCode.Text.ToString();
            string prov = clientProv.Text.ToString();
            string email = clientEmail.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string cc_name = clientCCname.Text.ToString();
            string cc_number = clientCCNumber.Text.ToString();
            string cc_exp = clientCCExp.Text.ToString();
            string cc_cvv = clientCCCvv.Text.ToString();
            double total = 0;
           
            Client client = new Client();
            client.ClientFname = first_name;
            client.ClientLname = last_name;
            client.ClientDOB = DOB;
            client.ClientAddress = address;
            client.ClientCity = city;
            client.ClientPostalCode = postalCode;
            client.ClientProv = prov;
            client.ClientEmail = email;
            client.ClientPhone = phone;

            ClientCC clientCC = new ClientCC();
            clientCC.Name = cc_name;
            clientCC.CC_number = cc_number;
            clientCC.CC_exp = cc_exp;
            clientCC.CC_cvv = cc_cvv;

            List<string> merchandiseItems = new List<string>();
            //got this code from the pizza example
            foreach (Control control in mContainer.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox merchandise = (CheckBox)control;
                    if (merchandise.Checked)
                    {
                        merchandiseItems.Add(merchandise.Text);
                    }
                }
            }
            //similar to pizza example but this is getting the value of the checkbox and adding it total.
            foreach (Control control in mContainer.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox merchandise = (CheckBox)control;
                    if (merchandise.Checked)
                    {
                        total += double.Parse(merchandise.Attributes["Value"]);
                    }
                }
            }

            List<String> items = new List<String> {};
            Merchandise newmerchandise = new Merchandise(items, total);
            newmerchandise.items = newmerchandise.items.Concat(merchandiseItems).ToList();
            newmerchandise.merchandiseTotal = total;

            Summary summary = new Summary(client, clientCC, newmerchandise);
            personalInfo.InnerHtml = summary.printSummary();
        }
    }
}
