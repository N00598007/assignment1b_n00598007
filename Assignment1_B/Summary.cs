﻿using System;
namespace Assignment1_B
{
    public class Summary
    {
        public Client client;
        public ClientCC clientCC;
        public Merchandise merchandise;
        
        public Summary(Client c, ClientCC cc, Merchandise m)
        {
            client = c;
            clientCC = cc;
            merchandise = m;
        }

        public string printSummary(){
            string summary = "<h1 id=summary>Checkout Summary</h1> <br>";
            summary += "Name: "+client.ClientFname+" "+client.ClientLname+"<br>";
            summary += "DOB: " +client.ClientDOB+"<br>";
            summary += "Address: " + client.ClientAddress + "<br>";
            summary += "City: " + client.ClientCity + "<br>";
            summary += "Postal Code: " + client.ClientPostalCode + "<br>";
            summary += "Province: " + client.ClientProv + "<br>";
            summary += "Email: " + client.ClientEmail + "<br>";
            summary += "Phone: " + client.ClientPhone + "<br><br>";
            summary += "Credit Card Info: <br>";
            summary += "Name: "+ clientCC.Name +"<br>";
            summary += "CC Number: "+ clientCC.CC_number +"<br>";
            summary += "CC Experation: "+ clientCC.CC_exp +"<br>" ;
            summary += "CVV: "+ clientCC.CC_cvv+ "<br><br>";
            summary += "Order Summary: <br>" + String.Join("<br>", merchandise.items.ToArray()) + "<br/> Sign Up Fee: $29.99<br><br>";
            summary += "TOTAL: $"+ calcTotal().ToString()+"<br><br><br>";

            return summary;
        }
        public double calcTotal(){
            double total = 0;

            total += (merchandise.merchandiseTotal + 29.99)*1.13;

            return Math.Round(total, 2);
        }
    }
}
