﻿using System;
namespace Assignment1_B
{
    public class ClientCC
    {
        private string name;
        private string cc_number;
        private string cc_exp;
        private string cc_cvv;

        public ClientCC()
        {
        }

        public string Name{
            get { return name; }
            set { name = value; }
        }
        public string CC_number{
            get { return cc_number; }
            set { cc_number = value; }
        }
        public string CC_exp{
            get { return cc_exp; }
            set { cc_exp = value; }
        }
        public string CC_cvv{
            get { return cc_cvv; }
            set { cc_cvv = value; }
        }
    }
}
