﻿<%@ Page Language="C#" Inherits="Assignment1_B.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Assignment1-B</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 id="gym">GYM MEMBERSHIP SIGN UP</h1>
            <div id="validation">
                <asp:validationsummary forecolor="Red" runat="server" id="validationSummary"></asp:validationsummary>
            </div>
            <div class="page-wrapper">
                <fieldset class="bank">
                <legend>BANK INFORMATION</legend>
                    <br>
                    <asp:Label runat="server" id="clientCCNameLabel">FULL NAME (as appear on cc)</asp:Label><br>
                    <asp:TextBox runat="server" id="clientCCname" placeholder="John Smith"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" id="ccName_validator" ErrorMessage="Required Full Name" ControlToValidate="clientCCname">***</asp:RequiredFieldValidator><br>
                    
                    <asp:Label runat="server" id="clientCCNumberLabel">CREDIT CARD NUMBER</asp:Label><br>
                    <asp:TextBox runat="server" id="clientCCNumber" placeholder="0000 0000 0000 0000"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" id="ccNumber_validator" ErrorMessage="Required Credit Card Number" ControlToValidate="clientCCNumber">***</asp:RequiredFieldValidator><br>
            
                    <asp:Label runat="server" id="clientCCExpLabel">EXPERATION DATE</asp:Label><br>
                    <asp:TextBox runat="server" id="clientCCExp" placeholder="MM/YY"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" id="ccExp_validator" ErrorMessage="Required Experation Date" ControlToValidate="clientCCExp">***</asp:RequiredFieldValidator><br>
                    
                    <asp:Label runat="server" id="clientCCCvvLabel">CVV</asp:Label><br>
                    <asp:TextBox runat="server" id="clientCCCvv" placeholder="123"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" id="ccCvv_validator" ErrorMessage="Required CVV" ControlToValidate="clientCCCvv">***</asp:RequiredFieldValidator><br>
                    
                    <asp:CheckBox runat="server" id="billingAddress" Text="Billing Address is same as Mailing Address"></asp:CheckBox><br>
                    <asp:CheckBox runat="server" id="agreement" Text="I authorize payment of monthly membership dues by automatic draft from"></asp:CheckBox> 
                 </fieldset><br>
                <fieldset class="content">
                     <legend>MEMBERSHIP PAYMENT INFORMATION</legend>
                        <p>ENROLLMENT FEE:        $20<br>
                           MONTHLY DUES AMOUNT:   $9.99<br>
                           DATE DRAFT BEGINS:     October 5, 2018<br><br>      
                           TOTAL: $29.99
                        </p>  
                </fieldset>
                <div id="mContainer" runat="server">
                    <fieldset class="content">
                        <legend>STORE MERCHANDISE</legend> 
                            <asp:CheckBox runat="server" id="merchandise1" Text="Gym T-Shirt (Free)" value="0.00"></asp:CheckBox><br>
                            <asp:CheckBox runat="server" id="merchandise2" Text="750 ml water bottle ($9.99)" value="9.99"></asp:CheckBox><br>
                            <asp:CheckBox runat="server" id="merchandise3" Text="6 Protein Bar ($7.99)" value="7.99"></asp:CheckBox><br>
                            <asp:CheckBox runat="server" id="merchandise4" Text="350 ml Strawberry Pre Workout Drink ($24.99)" value="24.99"></asp:CheckBox><br>
                            <asp:CheckBox runat="server" id="merchandise5" Text="5 LB Whey Protein ($109.99)" value="109.99"></asp:CheckBox><br>      
                    </fieldset>
                </div>
                    <fieldset id="clientInfo">
                    <legend>PERSONAL INFORMATION</legend>
                        <br>
                        <asp:Label runat="server" id="clientFNameLabel">FIRST NAME</asp:Label><br>
                        <asp:TextBox runat="server" id="clientFname"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="fname_validator" ErrorMessage="Required First Name" ControlToValidate="clientFname">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientLnameLabel">LAST NAME</asp:Label><br>
                        <asp:TextBox runat="server" id="clientLname"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="lname_validator" ErrorMessage="Required Last Name" ControlToValidate="clientLname">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientDOBLabel">DATE OF BIRTH</asp:Label><br>
                        <asp:TextBox runat="server" id="clientDOB" placeholder="MM/DD/YYYY"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="dob_validator" ErrorMessage="Required Date of Birth" ControlToValidate="clientDOB">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientAddressLabel">ADDRESS</asp:Label><br>
                        <asp:TextBox runat="server" id="clientAddress"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="address_validator" ErrorMessage="Required Address" ControlToValidate="clientAddress">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientCityLabel">CITY</asp:Label><br>
                        <asp:TextBox runat="server" id="clientCity"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="city_validator" ErrorMessage="Required City" ControlToValidate="clientCity">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientPostCodeLabel">POSTAL CODE</asp:Label><br>
                        <asp:TextBox runat="server" id="clientPostalCode" placeholder="A1A 1A1"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="pCode_validator" ErrorMessage="Required Postal Code" ControlToValidate="clientPostalCode">***</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "clientPostalCode" ID="ecpressionValidator" ValidationExpression = "(\D{1}\d{1}\D{1}\-?\d{1}\D{1}\d{1})|(\D{1}\d{1}\D{1}\ ?\d{1}\D{1}\d{1})" 
                                                        runat="server" ErrorMessage="Please Enter Valid Postal Code" ForeColor="Red">Please Enter Valid Postal Code</asp:RegularExpressionValidator><br>
                        <asp:Label runat="server" id="clientProvLabel">PROVINCE</asp:Label><br>
                        <asp:TextBox runat="server" id="clientProv"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="prov_validator" ErrorMessage="Required Province" ControlToValidate="clientProv">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientEmailLabel">EMAIL</asp:Label><br>
                        <asp:TextBox runat="server" id="clientEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="email_validator" ErrorMessage="Required Email" ControlToValidate="clientEmail">***</asp:RequiredFieldValidator><br>
                        
                        <asp:Label runat="server" id="clientPhoneLabel">PHONE</asp:Label><br>
                        <asp:TextBox runat="server" id="clientPhone"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" id="phone_validator" ErrorMessage="Required Phone Number" ControlToValidate="clientPhone">***</asp:RequiredFieldValidator><br>                 
                </fieldset><br>
                </div>
             
                <asp:Button CssClass="btn" id="submintBtn" runat="server" Text="Submit" OnClick="Submit" />
                <div runat="server" id="personalInfo">                      
            </div>   
        </div>
    </form>       
</body>
</html>
