﻿using System;
namespace Assignment1_B
{
    public class Client
    {
        private string clientFname;
        private string clientLname;
        private string clientDOB;
        private string clientAddress;
        private string clientCity;
        private string clientPostalCode;
        private string clientProv;
        private string clientEmail;
        private string clientPhone;

        public Client()
        {
        }

        public string ClientFname {
            get { return clientFname; }
            set { clientFname = value; }
        }
        public string ClientLname{
            get { return clientLname; }
            set { clientLname = value; }
        }
        public string ClientDOB{
            get { return clientDOB; }
            set { clientDOB = value; }
        }
        public string ClientAddress{
            get { return clientAddress; }
            set { clientAddress = value; }
        }
        public string ClientCity{
            get { return clientCity; }
            set { clientCity = value; }
        }
        public string ClientPostalCode{
            get { return clientPostalCode; }
            set { clientPostalCode = value; }
        }
        public string ClientProv{
            get { return clientProv; }
            set { clientProv = value; }
        }
        public string ClientEmail{
            get { return clientEmail; }
            set { clientEmail = value; }
        }
        public string ClientPhone{
            get { return clientPhone; }
            set { clientPhone = value; }
        }

    }
}
