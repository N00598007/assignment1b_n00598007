﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
namespace Assignment1_B
{
    public class Merchandise
    {
        public List<string> items;
        public double merchandiseTotal;

        public Merchandise(List<string> m, double mc)
        {
            items = m;
            merchandiseTotal = mc;
        }
    }
}
